export function fight(firstFighter, secondFighter) {
    let firstFighterHealth = firstFighter.health;
    let secondFighterHealth = secondFighter.health;

    while (true) {
        firstFighterHealth -= getDamage(secondFighter, firstFighter);
        if (secondFighterHealth <= 0) {
            return secondFighter;
        }

        secondFighterHealth -= getDamage(firstFighter, secondFighter);
        if (secondFighterHealth <= 0) {
            return firstFighter;
        }
    }
}

export function getDamage(attacker, enemy) {
    const hitPower = getHitPower(attacker);
    const blockPower = getBlockPower(enemy);

    const damage = hitPower - blockPower;
    return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
    const attack = fighter.attack;
    const criticalHitChance = Math.random() + 1;
    return attack * criticalHitChance;
}

export function getBlockPower(fighter) {
    const defense = fighter.defense;
    const dodgeChance = Math.random() + 1;
    return defense * dodgeChance;
}
