import {createElement} from "../helpers/domHelper";
import {createImage} from "./fighterDetails";
import {showModal} from "./modal";

export function showWinnerModal(fighter) {
    //hideFighters();

    const winnerElement = createElement({tagName: "div", className: "modal-body"});
    const nameElement = createElement({tagName: "span", className: "fighter-name"});
    nameElement.innerText = fighter.name;
    const imageElement = createImage(fighter.source);
    winnerElement.append(nameElement, imageElement);

    showModal({title: "Winner", bodyElement: winnerElement });
}

function hideFighters() {
    const fighters = document.querySelector("div#root > .fighters");
    fighters.remove();
}
