import {createElement} from '../helpers/domHelper';
import {showModal} from './modal';
import {createFighterElement} from "../fightersView";

export function showFighterDetailsModal(fighter) {
    const title = 'Fighter info';
    const bodyElement = createFighterDetails(fighter);
    showModal({title, bodyElement});
}

function createFighterDetails(fighter) {
    const fighterDetails = createElement({tagName: 'div', className: 'modal-body'});
    const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
    nameElement.innerText = fighter.name;
    const attackElement = createFighterElement("attack", fighter.attack);
    const defenseElement = createFighterElement("defense", fighter.defense);
    const healthElement = createFighterElement("health", fighter.health);
    const imageElement = createImage(fighter.source);

    fighterDetails.append(nameElement, attackElement, defenseElement, healthElement, imageElement);
    return fighterDetails;
}

export function createImage(source) {
    const attributes = {src: source};
    const imgElement = createElement({tagName: 'img', className: 'fighter-image', attributes});

    return imgElement;
}


